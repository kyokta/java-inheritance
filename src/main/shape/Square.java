package main.shape;

public class Square extends Rectangle{
    public Square() {
    }

    public Square(double side) {
        super.setWidth(side);
    }

    public Square(String color, boolean filled,  double side) {
        super.setColor(color);
        super.setFilled(filled);
        super.setWidth(side);
    }
    public double getSide(){
        return super.getWidth();
    }
    public void setSide(double side){
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return "Square{}";
    }
}
