package main.person;

public class App {
    public static void main(String[] args) {
        Person person = new Person("Afrian", "Jalan Lembu Pala");
        Student student = new Student("Rizky", "Jalan Kaliurang", "TRPL", 2022, 500000);
        Staff staff = new Staff("Oktarinanto", "Jalan Cangkringan", "SMK N 2 Depok Sleman", 100000);

        System.out.println("----------Person----------");
        System.out.println(person.getName());
        System.out.println("Old Address :" + person.getAddress());
        person.setAddress("Jakal");
        System.out.println("New Address :" + person.getAddress());
        System.out.println(person.toString());
        System.out.println("--------------------------" + "\n");

        System.out.println("----------Student----------");
        System.out.println(student.getName());
        System.out.println("Old Address :" + student.getAddress());
        student.setAddress("Jalan Lembu Pala");
        System.out.println("New Address :" + student.getAddress());
        System.out.println("Old Program :" + student.getProgram());
        student.setProgram("RPL");
        System.out.println("New Program :" + student.getProgram());
        System.out.println("Old Year :" + student.getYear());
        student.setYear(2021);
        System.out.println("New Year :" + student.getYear());
        System.out.println("Old Year :" + student.getFee());
        student.setYear(100);
        System.out.println("New Year :" + student.getFee());
        System.out.println(student.toString());
        System.out.println("---------------------------" + "\n");

        System.out.println("----------Staff----------");
        System.out.println(staff.getName());
        System.out.println("Old Address :" + staff.getAddress());
        staff.setAddress("Jalan Magelang");
        System.out.println("New Address :" + staff.getAddress());
        System.out.println("Old School :" + staff.getSchool());
        staff.setSchool("UGM");
        System.out.println("New School :" + staff.getSchool());
        System.out.println("Old Pay :" + staff.getPay());
        staff.setPay(200);
        System.out.println("New Pay :" + staff.getPay());
        System.out.println(staff.toString());
        System.out.println("-------------------------" + "\n");
    }
}
