package main.circle;

public class App {
    public static void main(String[] args) {
        Circle lingkaran = new Circle();
        System.out.println("Lingkaran :");
        System.out.println("Radius awal :" + lingkaran.getRadius());
        System.out.println("Warna awal :" + lingkaran.getColor());
        lingkaran.setRadius(7);
        lingkaran.setColor("Blue");
        System.out.println("Current radius :" + lingkaran.getRadius());
        System.out.println("Current color :" + lingkaran.getColor());
        System.out.println("Area : " + lingkaran.getArea());

        System.out.println("\n");

        Cylinder tabung = new Cylinder();
        System.out.println("Tabung :");
        System.out.println("Area :" + tabung.getArea());
        System.out.println("Height :" + tabung.getHeight());
        tabung.setHeight(20);
        System.out.println("Current height : " + tabung.getHeight());
        System.out.println("Volume : "+ tabung.getVolume());
    }
}
